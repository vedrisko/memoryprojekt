<?php
	$level = (int)$_GET['level'];
	$images = (int)$_GET['images'];
	$user =	$_GET['user'];

	if( $level == 1 )
		$imagnum = rand(3,6);
	else if( $level == 2 )
		$imagnum = rand(7,10);
	else if( $level == 3 )
		$imagnum = rand(13,20);

	$allImages = [];
	$imagesArray = [];

	if( $images == 1 )
		$dirname = "img/";
	else
		$dirname = "Image/".$user."/";

	$folder = opendir($dirname);
	while( $file = readdir( $folder ) ){
            if($file !== '.' && $file !== '..'){
                array_push( $allImages, $dirname.$file );
            }
        }

    if( sizeof($allImages) < $imagnum ){
    	echo json_encode( "Nemate dovoljno slika u folderu da bi igrali za odabranu razinu." );
    }
    else{

	    shuffle($allImages);
	    for( $i = 0; $i < $imagnum; ++$i )
	    {
	    	array_push( $imagesArray, $allImages[$i] );
	    	array_push( $imagesArray, $allImages[$i] );
	    }

		shuffle($imagesArray);
		echo json_encode($imagesArray);
	}
?>
