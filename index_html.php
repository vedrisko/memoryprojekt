<?php
function izbor()
{
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="css/custom.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<title>Choose</title>
	</head>
	<body>
	<header id="top" class="header">
        <div class="text-vertical-center">
	<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
	<button type="submit" class="btn btn-default navbar-btn" name="gumb" value="log">Želim se ulogirati</button>
	<button type="submit" class="btn btn-default navbar-btn" name="gumb" value="sig">Želim se registrirati</button>
	</form>
	 </div>
    </header>
	</body>
	</html>
<?php
}
// Iscrtava formu za ulogiravanje / kreiranje noviog korisnika.
function crtaj_loginForma( $message = '' )
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="css/custom.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<title>Login</title>
	</head>
	<body>
		<header id="top" class="header">
        <div class="text-vertical-center">
        	<div class="col-lg-4 col-lg-offset-4">
		<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
			<input type="text" class="form-control" name="username" placeholder="Korisničko ime"/>
			<br />
			<input type="password" class="form-control" name="password" placeholder="Password"/>
			<br />
			<button type="submit" class="btn btn-default navbar-btn" name="gumb" value="login">Ulogiraj se!</button>
			<br > <br >
			<button type="submit" class="btn btn-default navbar-btn" name="gumb" value="sig">Želim se registrirati!</button>
		</form>

		<?php
			if( $message !== '' )
				echo '<p class="bg-info">' . $message . '</p>';
		?>
	</div>
	</div>
</header>
	</body>
	</html>
	<?php
}



function crtaj_signupForma( $message = '' )
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf8" />
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	  <link rel="stylesheet" href="css/custom.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<title>Signup</title>
	</head>
	<body>
		<header id="top" class="header">
        <div class="text-vertical-center">
        	<div class="col-lg-4 col-lg-offset-4">
		<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
			<input type="text" class="form-control" name="name" id="name" placeholder="Ime"/>
			<br >
			<input type="text" class="form-control" name="username" id="username" placeholder="Korisničko ime"/>
			<br />
			<input type="password" class="form-control" name="password" id="pass" placeholder="Password"/> <p id="strength"></p>
			<br />
			<button type="submit" class="btn btn-default navbar-btn" name="gumb" id="signUp" value="novi">Registriraj se!</button>
			<br ><br >
			<button type="submit" class="btn btn-default navbar-btn" name="gumb" value="log">Želim se ulogirati</button>
		</form>
		<p class="bg-info" id="invalid"></p>
		<?php
			if( $message !== '' )
				echo '<p class="bg-info">' . $message . '</p>';
		?>
	</div>
	</div>
</header>
	</body>

	<script type="text/javascript">

	$("#name").on("input", function() {
		var testName = /^[A-Z][a-z]{1,20}$/;
		var n = $("#name").val();
		if( testName.test(n) ) {
			$("#invalid").html("");
			$("#signUp").prop( "disabled", "" );
		}
		else{
			$("#invalid").html( "Ime se sastoji od najmanje 2 slova i prvo slovo imena je uvijek veliko." );
			$("#signUp").prop( "disabled", "disabled" );
		}
	});

	$("#username").on("input", function() {
		var un = $(this).val();
		if( testUserName.test(un) ) {
			$("#invalid").html("");
			$("#signUp").prop( "disabled", "" );
		}
		else{
			$("#invalid").html( "Korisničko ime se sastoji od najmanje 2 slova a najviše 20 slova velikh ili malih slova." );
			$("#signUp").prop( "disabled", "disabled" );
		}
	});

	$("#pass").on("input", function() {
		var pass = $(this).val();
		if( pass.length < 5)
			$("#strength").html("weak");
		else if( pass.length > 4 && pass.length < 8 )
			$("#strength").html("good");
		else if( pass.length > 7 )
			$("#strength").html("excelent");
	});


	</script>

	</html>
	<?php
}

?>
