$(document).ready(function() {

  //prva i druga okrenuta slika i brojac za njih
  var firstImg = "";
  var secondImg = "";
  var count = 0;

  //ukoliko je pritisnut elem li polju za igru treba pokazati koja je slika na tom 'mjestu'
  $("#imageDiv").on('click', 'li', function() {
    
    if ( (count < 2) && ( $(this).children("img").hasClass("turned") ) === false ) {
      
      count++;
      $(this).children("img").show();
      $(this).children("img").addClass("turned");
      
      if (count === 1 ) { 
        firstImg = $(this).children("img").attr("src"); 
      }   
      else { 
        secondImg = $(this).children("img").attr("src"); 
        
        if ( firstImg === secondImg ) { 
          $( "li" ).children("img[src='" + secondImg + "']").addClass("goal");
        } 
        else { 
          setTimeout(function() {
            $("img").not(".goal").hide();
            $("img").not(".goal").removeClass("turned");
          }, 1000);
        }
        
        count = 0;       
      }
    }
  });

});


